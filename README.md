<!--
SPDX-FileCopyrightText: 2020 Tristan Touileb <tristan@p2g.me>
SPDX-FileCopyrightText: 2020 Adriel Dumas--Jondeau <adrieldj@orange.fr>

SPDX-License-Identifier: WTFPL
-->

# taptempo-zig

![Licensed under the WTFPL](http://www.wtfpl.net/wp-content/uploads/2012/12/wtfpl-badge-4.png)

Implémentation en [zig](https://ziglang.org/) de [taptempo](https://linuxfr.org/wiki/taptempo).
