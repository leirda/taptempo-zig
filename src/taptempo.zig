// SPDX-FileCopyrightText: 2020 Tristan Touileb <tristan@p2g.me>
// SPDX-FileCopyrightText: 2020 Adriel Dumas--Jondeau <adrieldj@orange.fr>

// SPDX-License-Identifier: WTFPL

const std = @import("std");
const ArrayList = std.ArrayList;
const Timer = std.time.Timer;

const testing = std.testing;

pub fn TapTempo(sampleSize: u64, resetTime: u64, precision: u64) type {
    return struct {
        const Self = @This();

        records: ArrayList(f64),
        timer: Timer,
        bpm: f64,

        pub fn start() !Self {
            const records = ArrayList(f64).init(std.heap.page_allocator);
            defer records.deinit();

            return Self{
                .records = records,
                .timer = try Timer.start(),
                .bpm = std.math.inf(f64),
            };
        }

        pub fn tap(self: *Self) !void {
            try self.appendSeconds();

            self.bpm = computeBpm(
                if (self.records.items.len > sampleSize) self.records.orderedRemove(0) else self.records.items[0],

                self.records.items[self.records.items.len - 1],

                @intToFloat(f64, self.records.items.len),
            );
        }

        fn computeBpm(first: f64, last: f64, count: f64) f64 {
            return 60.0 / (last - first) * count;
        }

        pub fn printBpm(self: Self, writer: anytype) !void {
            // TODO: Use wrapper around formatType when this will become
            // available in the language.
            try std.fmt.formatType(self.bpm, "d", std.fmt.FormatOptions{ .precision = precision }, writer, 3);
        }

        fn appendSeconds(self: *Self) !void {
            try self.records.append(@intToFloat(f64, self.timer.read()) / @as(f64, std.time.ns_per_s));
        }
    };
}
