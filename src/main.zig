// SPDX-FileCopyrightText: 2020 Tristan Touileb <tristan@p2g.me>
// SPDX-FileCopyrightText: 2020 Adriel Dumas--Jondeau <adrieldj@orange.fr>

// SPDX-License-Identifier: WTFPL

const TapTempo = @import("./taptempo.zig").TapTempo;
const std = @import("std");
const time = std.time;

pub fn main() !void {
    const in = std.io.getStdIn().reader();
    const out = std.io.getStdOut().writer();

    const sampleSize = 5;
    const resetTime = 5;
    const precision = 0;

    var taptempo = try TapTempo(sampleSize, resetTime, precision).start();

    while (in.readByte()) |char| {
        if (char == 'q') {
            break;
        }

        try taptempo.tap();
        try taptempo.printBpm(out);
        try out.print("\n", .{});
    } else |EOF| {}

    try out.print("Exiting...\n", .{});
}
